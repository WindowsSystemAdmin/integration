LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := 3rd-party-test
LOCAL_SRC_FILES := hello.c
LOCAL_LDLIBS := -liniparser

include external/gentoo/integration/3rd-party-binary.mk

include $(CLEAR_VARS)

LOCAL_MODULE := 3rd-party-test-cxx
LOCAL_SRC_FILES := test-cxx.cc
LOCAL_CPP_EXTENSION := .cc

include external/gentoo/integration/3rd-party-binary.mk
