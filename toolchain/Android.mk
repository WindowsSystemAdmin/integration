LOCAL_PATH := $(call my-dir)

EXTERNAL_PACKAGES := $(dir $(LOCAL_PATH))/external-packages
EXTERNAL_PACKAGES_INCLUDE := $(EXTERNAL_PACKAGES)/include

include $(CLEAR_VARS)

LOCAL_MODULE := 3rd-party-gcc
LOCAL_MODULE_CLASS := EXECUTABLES
LOCAL_IS_HOST_MODULE := true

include $(BUILD_SYSTEM)/base_rules.mk

compiler = $(intermediates)/$(LOCAL_MODULE)
$(compiler): $(LOCAL_PATH)/$(LOCAL_MODULE).in | $(3RD_PARTY_SYSTEM_DEPS)
	$(hide)mkdir -p $(dir $@)
	$(hide)sed \
		-e 's:@CC@:$(notdir $(TARGET_CC)):' \
		-e 's:@CFLAGS@:$(foreach p,$(TARGET_C_INCLUDES) $(EXTERNAL_PACKAGES_INCLUDE),-isystem "$${ANDROID_BUILD_TOP}/$(p)") $(filter-out -Werror=%,$(TARGET_GLOBAL_CFLAGS)):' \
		-e 's:@LDFLAGS@:-B"$(patsubst $(PRODUCT_OUT)/%,$${ANDROID_PRODUCT_OUT}/%,$(TARGET_OUT_INTERMEDIATE_LIBRARIES))":' \
		-e 's:@ROOT_SUBDIR@:$(3RD_PARTY_ROOT_SUBDIR):g' \
		-e 's:@LIBDIR@:$(3RD_PARTY_LIBDIR):g' \
		$< > $@.tmp \
		&& chmod a+rx $@.tmp && mv $@.tmp $@

LOCAL_BUILT_MODULE = $(compiler)
LOCAL_GENERATED_SOURCES += $(3RD_PARTY_CC)

3RD_PARTY_WRAPPERS += $(HOST_OUT_EXECUTABLES)/$(LOCAL_MODULE)

include $(CLEAR_VARS)

LOCAL_MODULE := 3rd-party-g++
LOCAL_MODULE_CLASS := EXECUTABLES
LOCAL_IS_HOST_MODULE := true

include $(BUILD_SYSTEM)/base_rules.mk

compiler = $(intermediates)/$(LOCAL_MODULE)
$(compiler): $(LOCAL_PATH)/$(LOCAL_MODULE).in | $(3RD_PARTY_SYSTEM_DEPS)
	$(hide)mkdir -p $(dir $@)
	$(hide)sed \
		-e 's:@CXX@:$(notdir $(TARGET_CXX)):' \
		-e 's:@CXXFLAGS@:$(foreach p,$(TARGET_C_INCLUDES) external/libcxx/include external/libcxxabi/include,-isystem "$${ANDROID_BUILD_TOP}/$(p)") $(filter-out -Werror=%,$(TARGET_GLOBAL_CFLAGS)):' \
		-e 's:@LDFLAGS@:-B"$(patsubst $(PRODUCT_OUT)/%,$${ANDROID_PRODUCT_OUT}/%,$(TARGET_OUT_INTERMEDIATE_LIBRARIES))":' \
		-e 's:@ROOT_SUBDIR@:$(3RD_PARTY_ROOT_SUBDIR):g' \
		-e 's:@LIBDIR@:$(3RD_PARTY_LIBDIR):g' \
		$< > $@.tmp \
		&& chmod a+rx $@.tmp && mv $@.tmp $@

LOCAL_BUILT_MODULE = $(compiler)
LOCAL_GENERATED_SOURCES += $(3RD_PARTY_CXX)

3RD_PARTY_WRAPPERS += $(HOST_OUT_EXECUTABLES)/$(LOCAL_MODULE)

include $(CLEAR_VARS)

LOCAL_MODULE := 3rd-party-pkg-config
LOCAL_MODULE_CLASS := EXECUTABLES
LOCAL_IS_HOST_MODULE := true

include $(BUILD_SYSTEM)/base_rules.mk

compiler = $(intermediates)/$(LOCAL_MODULE)
$(compiler): $(LOCAL_PATH)/$(LOCAL_MODULE).in
	$(hide)mkdir -p $(dir $@)
	$(hide): $(PRODUCT_OUT) $(TARGET_OUT_INTERMEDIATE_LIBRARIES)
	$(hide)sed \
		-e 's:@ROOT_SUBDIR@:$(3RD_PARTY_ROOT_SUBDIR):g' \
		$< > $@.tmp \
		&& chmod a+rx $@.tmp && mv $@.tmp $@

LOCAL_BUILT_MODULE = $(compiler)
LOCAL_GENERATED_SOURCES += $(3RD_PARTY_PKG_CONFIG)

3RD_PARTY_WRAPPERS += $(HOST_OUT_EXECUTABLES)/$(LOCAL_MODULE)
